# walks

This is a simple Go package, that contains functions to walk directory structure and perform user-defined actions on files and directories.

## Download

To get this package, run the following command

```sh
go get -v gitlab.com/utt_meelis/walks
```

## Author

Meelis Utt
